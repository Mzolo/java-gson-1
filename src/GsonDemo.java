import com.google.gson.*;

import java.lang.reflect.Type;

public class GsonDemo {
    static class Person {
        String name;
        int age;
    Person(String name, int age) {
        this.name = name;
        this.age = age;
        }

    @Override
    public String toString() {
        return name + ": " + age;
        }
    }

    public static void main(String[] args) {

        class PersonDeserializer implements JsonDeserializer<Person>{
            @Override
            public Person deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            {
                JsonObject jsonObject = json.getAsJsonObject();
                String firstName = jsonObject.get("first-name").getAsString();
                String lastName = jsonObject.get("last-name").getAsString();
                int age = jsonObject.getAsJsonPrimitive("age").getAsInt();
                String address = jsonObject.get("address").getAsString();
                return new Person(firstName + " " + lastName, 45);
            }
        }
        // Parsing a JSON Object into a Java Object via a Custom Deserializer
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Person.class, new PersonDeserializer());
        Gson gson = gsonBuilder.create();
        // Gson gson = new Gson();
        String json = "{ first-name: \"John\", last-name: \"Davis\", " + " age: 45, address: \"Box-1\" }";
        // String json = "{ name: \"John Davis\", age: 45 }";
        Person person = gson.fromJson(json, Person.class);
        System.out.println(person);

        // Creating JSON Object from a java Object
        Person p = new Person("Fred Brisset", 62);
        Gson googleGson = new Gson();
        String otherjson = googleGson.toJson(p);
        System.out.println(otherjson);

    }

} // end of class
